import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { withRouter } from "react-router-dom";
import axios from "axios";

function Schedule(props) {
  const [data, setData] = useState({});
  const [modalShow, setModalShow] = useState(false);

  useEffect(() => {
    const id = props.dataId;
    const start_date = formatDateQuery(one);
    const end_date = formatDateQuery(seven);

    axios
      // .get(`http://192.168.100.8:2000/${id}/${start_date}/${end_date}`)
      .get(`https://nodejs.alphacreativee.com/${id}/${start_date}/${end_date}`)
      .then(async (response) => {
        const result = await response.data.data;
        // console.log(result);
        setData(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [props]);

  // console.log(days[d.getDay() + 4]);

  var one = new Date();
  one.setDate(one.getDate());
  var two = new Date();
  two.setDate(two.getDate() + 1);
  var three = new Date();
  three.setDate(three.getDate() + 2);
  var four = new Date();
  four.setDate(four.getDate() + 3);
  var five = new Date();
  five.setDate(five.getDate() + 4);
  var six = new Date();
  six.setDate(six.getDate() + 5);
  var seven = new Date();
  seven.setDate(seven.getDate() + 6);
  var days = ["MINGGU", "SENIN", "SELASA", "RABU", "KAMIS", "JUMAT", "SABTU"];

  function formatDateQuery(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();

    var dateQuery = `${yyyy}-${mm}-${dd}`;

    return dateQuery;
  }

  function formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();

    var dateIndo = `${dd}/${mm}/${yyyy}`;

    return dateIndo;
  }

  // function formatTime() {
  var time = new Date();
  var hours = time.getHours() + 3;
  var minutes = time.getMinutes();
  var seconds = time.getSeconds();

  var timeIndo = `${hours}:${minutes}:${seconds}`;

  if (data.length > 0) {
    return (
      <div>
        <div className="col-md-12">Hello {props.name}</div>
        <hr />
        <MyVerticallyCenteredModal
          show={modalShow}
          onHide={() => setModalShow(false)}
        />
        <div className="col-md-12">
          <div className="table-responsive">
            <div className="card-profile-stats d-flex">
              <div style={{ padding: "0px" }}></div>
              <div className="btn btn-neutral">
                <span className="heading" style={{ color: "green" }}>
                  {days[one.getDay()]}
                </span>
                <span style={{ color: "red" }}>{formatDate(one)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[one.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.time.time <= timeIndo ? (
                            <p>CLOSED</p>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() => setModalShow(true)}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              {/* END FIRST */}
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div className="btn btn-neutral">
                <span className="heading" style={{ color: "green" }}>
                  {days[two.getDay()]}
                </span>
                <span style={{ color: "red" }}>{formatDate(two)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[two.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() => setModalShow(true)}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              {/* END SELASA */}
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div className="btn btn-neutral">
                <span className="heading" style={{ color: "green" }}>
                  {days[three.getDay()]}
                </span>
                <span style={{ color: "red" }}>{formatDate(three)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[three.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() => setModalShow(true)}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div className="btn btn-neutral">
                <span className="heading" style={{ color: "green" }}>
                  {days[four.getDay()]}
                </span>
                <span style={{ color: "red" }}>{formatDate(four)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[four.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() => setModalShow(true)}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div className="btn btn-neutral">
                <span className="heading" style={{ color: "green" }}>
                  {days[five.getDay()]}
                </span>
                <span style={{ color: "red" }}>{formatDate(five)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[five.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() => setModalShow(true)}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div className="btn btn-neutral">
                <span className="heading" style={{ color: "green" }}>
                  {days[six.getDay()]}
                </span>
                <span style={{ color: "red" }}>{formatDate(six)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[six.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() => setModalShow(true)}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div className="btn btn-neutral">
                <span className="heading" style={{ color: "green" }}>
                  {days[seven.getDay()]}
                </span>
                <span style={{ color: "red" }}>{formatDate(seven)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[seven.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() => setModalShow(true)}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <p>Loading...</p>;
}

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Modal heading
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Centered Modal</h4>
        <p>
          Lorem ipsum dolor sit amet Cras mattis consectetur purus sit amet
          fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget
          quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default withRouter(Schedule);
