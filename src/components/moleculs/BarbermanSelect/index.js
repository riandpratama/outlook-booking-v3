import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import axios from "axios";
import { useHistory, withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";

const BarbermanSelect = (props) => {
  const dispatch = useDispatch();
  const [locationId, setLocationId] = useState(1);
  const history = useHistory();
  const serviceByBarberman = props.barbermanService;
  const isGender = props.genderAvailable;
  // console.log(isGender)
  useEffect(() => {
    const getLocalStorage = localStorage.getItem("customerOutlook");
    const localstorageParse = JSON.parse(getLocalStorage);
    const token = localstorageParse.token;

    axios
      .get(
        process.env.REACT_APP_API_PATH +
          `/barberman/${locationId}/${serviceByBarberman}?gender=${isGender}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(async (response) => {
        const result = await response.data.data;
        dispatch({ type: "UPDATE_DATA_BARBERMAN", payload: result });
      })
      .catch((err) => {
        console.log(err);
      });
  }, [locationId, dispatch, serviceByBarberman, isGender]);

  const onSubmit = (e) => {
    setLocationId(e.target.value);
  };

  return (
    <>
      <Container className="d-flex align-items-center space-between" fluid>
        <Row className="mt-3">
          <Col xs="6"></Col>
          <Col xs="6">
            {" "}
            <span className="text-white"> Pilih Lokasi: </span>
          </Col>
          <Col xs="6">
            <img
              alt="Back"
              height="25px"
              src={require("../../../assets/local/backFull.png").default}
              onClick={() => history.goBack()}
            />
          </Col>
          <Col xs="6">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <img
                    alt="Back"
                    width="25px"
                    src={
                      require("../../../assets/local/selectLocation.png")
                        .default
                    }
                  />
                </InputGroupText>
              </InputGroupAddon>
              <Input type="select" onChange={onSubmit}>
                <option value="1">Sukodono</option>
                <option value="2">Geluran</option>
              </Input>
            </InputGroup>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default withRouter(BarbermanSelect);
