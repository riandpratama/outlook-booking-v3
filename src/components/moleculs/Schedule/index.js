import React, { useState, useEffect } from "react";
import { useHistory, withRouter, useParams } from "react-router-dom";
import axios from "axios";
import { Button, Spinner, Container, Row } from "reactstrap";

function Schedule(props) {
  const history = useHistory();

  const [data, setData] = useState({});

  var one = new Date();
  one.setDate(one.getDate());
  var two = new Date();
  two.setDate(two.getDate() + 1);
  var three = new Date();
  three.setDate(three.getDate() + 2);
  var four = new Date();
  four.setDate(four.getDate() + 3);
  var five = new Date();
  five.setDate(five.getDate() + 4);
  var six = new Date();
  six.setDate(six.getDate() + 5);
  var seven = new Date();
  seven.setDate(seven.getDate() + 6);
  var days = ["MINGGU", "SENIN", "SELASA", "RABU", "KAMIS", "JUMAT", "SABTU"];

  function formatDateQuery(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();

    var dateQuery = `${yyyy}-${mm}-${dd}`;

    return dateQuery;
  }

  useEffect(() => {
    const id = props.dataId;
    let oneDate = new Date();
    oneDate.setDate(oneDate.getDate());
    const start_date = formatDateQuery(oneDate);

    var sevenDate = new Date();
    sevenDate.setDate(sevenDate.getDate() + 6);
    const end_date = formatDateQuery(sevenDate);

    const datalocalstorage = localStorage.getItem("customerOutlook");
    const localstorageParse = JSON.parse(datalocalstorage);
    const token = localstorageParse.token;

    axios
      .get(
        process.env.REACT_APP_API_PATH +
          `/schedule/${id}/${start_date}/${end_date}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(async (response) => {
        const result = await response.data.data;
        setData(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [props]);

  function formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();

    var dateIndo = `${dd}/${mm}/${yyyy}`;

    return dateIndo;
  }

  // function formatTime() {
  var time = new Date();
  var hours = time.getHours() + 3;
  var minutes = time.getMinutes();
  var seconds = time.getSeconds();

  var timeIndo = `${hours}:${minutes}:${seconds}`;
  let { id } = useParams();
  console.log(id);
  if (data.length > 0) {
    return (
      <div>
        <div className="col-md-12">
          <div className="table-responsive">
            <div className="card-profile-stats d-flex">
              <div style={{ padding: "0px" }}></div>
              <div
                className="button-menu-barberman"
                style={{ borderRadius: "10px" }}
              >
                <span className="heading text-white">{days[one.getDay()]}</span>
                <span className="text-white">{formatDate(one)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[one.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.status === 0 ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.time.time <= timeIndo ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() =>
                                history.push(
                                  `/haircut/detail/${item.id}/${id}`,
                                  formatDateQuery(one)
                                )
                              }
                              id={item.id}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              {/* END FIRST */}
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div
                className="button-menu-barberman"
                style={{ borderRadius: "10px" }}
              >
                <span className="heading text-white">{days[two.getDay()]}</span>
                <span className="text-white">{formatDate(two)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[two.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.status === 0 ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() =>
                                history.push(
                                  `/haircut/detail/${item.id}/${id}`,
                                  formatDateQuery(two)
                                )
                              }
                              id={item.id}
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              {/* END SELASA */}
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div
                className="button-menu-barberman"
                style={{ borderRadius: "10px" }}
              >
                <span className="heading text-white">
                  {days[three.getDay()]}
                </span>
                <span className="text-white">{formatDate(three)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[three.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.status === 0 ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block "
                              variant="primary"
                              onClick={() =>
                                history.push(
                                  `/haircut/detail/${item.id}/${id}`,
                                  formatDateQuery(three)
                                )
                              }
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div
                className="button-menu-barberman"
                style={{ borderRadius: "10px" }}
              >
                <span className="heading text-white">
                  {days[four.getDay()]}
                </span>
                <span className="text-white">{formatDate(four)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[four.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.status === 0 ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() =>
                                history.push(
                                  `/haircut/detail/${item.id}/${id}`,
                                  formatDateQuery(four)
                                )
                              }
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div
                className="button-menu-barberman"
                style={{ borderRadius: "10px" }}
              >
                <span className="heading text-white">
                  {days[five.getDay()]}
                </span>
                <span className="text-white">{formatDate(five)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[five.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.status === 0 ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() =>
                                history.push(
                                  `/haircut/detail/${item.id}/${id}`,
                                  formatDateQuery(five)
                                )
                              }
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div
                className="button-menu-barberman"
                style={{ borderRadius: "10px" }}
              >
                <span className="heading text-white">{days[six.getDay()]}</span>
                <span className="text-white">{formatDate(six)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[six.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                          {item.status === 0 ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() =>
                                history.push(
                                  `/haircut/detail/${item.id}/${id}`,
                                  formatDateQuery(six)
                                )
                              }
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
              <div style={{ padding: "0px", margin: "3px" }}></div>
              <div
                className="button-menu-barberman"
                style={{ borderRadius: "10px" }}
              >
                <span className="heading text-white">
                  {days[seven.getDay()]}
                </span>
                <span className="text-white">{formatDate(seven)}</span>
                <span className="description">
                  {data
                    .filter((item) => item.day.name === days[seven.getDay()])
                    .map((item, index) => (
                      <div key={index}>
                        <div className="mt-3 mb-0 text-sm">
                        {item.status === 0 ? (
                            <Button
                              className="btn btn-sm btn-block btn-danger"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          ) : item.booking_date == null ? (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="primary"
                              onClick={() =>
                                history.push(
                                  `/haircut/detail/${item.id}/${id}`,
                                  formatDateQuery(six)
                                )
                              }
                            >
                              {item.time.time}
                            </Button>
                          ) : (
                            <Button
                              className="btn btn-sm btn-block"
                              variant="danger"
                            >
                              {item.time.time}
                            </Button>
                          )}
                        </div>
                      </div>
                    ))}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  return (
    <Container style={{ paddingTop: 50 }}>
      <Row className="justify-content-center">
        <Spinner style={{ width: "3rem", height: "3rem" }} />
      </Row>
    </Container>
  );
}

export default withRouter(Schedule);
