import React from "react";
import { Button, Card, Table, Col, Container, Row, Spinner } from "reactstrap";
import { useSelector, useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";

const BarbermanItem = (props) => {
  const stateGlobal = useSelector((state) => state.dataBarberman);
  const dispatch = useDispatch();

  const serviceByBarberman = props.barbermanService;

  const rupiah = (bilangan) => {
    var reverse = bilangan.toString().split("").reverse().join(""),
      ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join(".").split("").reverse().join("");

    return ribuan;
  };

  const setActiveTutorial = (nameProp, idData) => {
    const dataSubmitBarberman = {
      id: idData,
      name: nameProp,
      result: true,
    };

    dispatch({
      type: "UPDATE_DATA_SUBMIT_BARBERMAN",
      payload: dataSubmitBarberman,
    });
    setTimeout(() => {
      window.scroll({
        top: document.body.offsetHeight,
        left: 0,
        behavior: "smooth",
      });
    }, 500);
  };
  // console.log(stateGlobal);
  return (
    <>
      <Col className="mt-3" xl="3">
        <Card className="button-menu border-0">
          <h2 className="pl-3 pt-3 text-white">
            Barberman{" "}
            {(
              stateGlobal.length > 0
                ? stateGlobal[0].location_id === 1
                : "Sukodono"
            )
              ? "Sukodono"
              : "Geluran"}
          </h2>
          <div
            style={{
              borderBottom: "3px solid #BC9B64",
            }}
          ></div>
          {stateGlobal.length > 0 ? (
            <Table className="align-items-center table-flush" responsive hover>
              <tbody>
                {stateGlobal.map((item, index) => (
                  <tr key={index}>
                    <th scope="row" className="text-center ml-3 mr-3">
                      <Button
                        className="button-menu-barberman border-0 btn-block"
                        onClick={() => setActiveTutorial(item.name, item.id)}
                      >
                        <img
                          alt=""
                          src={process.env.REACT_APP_IMAGE_PATH + item.photo}
                          className="avatar rounded-circle mr-3"
                          style={{
                            height: "35px",
                            width: "35px",
                            float: "left",
                          }}
                        />
                        <span
                          style={{
                            float: "left",
                            paddingLeft: "10px",
                            color: "white",
                            marginTop: "10px",
                          }}
                        >
                          {item.name}
                        </span>
                        <span
                          className="align-items-center"
                          style={{ float: "right", marginTop: "10px" }}
                        >
                          <span style={{ color: "white" }}>
                            {" "}
                            {serviceByBarberman === "1"
                              ? `Rp.${rupiah(item.price_discount)}`
                              : ""}
                          </span>
                        </span>
                      </Button>
                    </th>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <Container style={{ paddingTop: 50 }}>
              <Row className="justify-content-center">
                <Spinner style={{ width: "3rem", height: "3rem" }} />
              </Row>
            </Container>
          )}
        </Card>
      </Col>
    </>
  );
};

export default withRouter(BarbermanItem);
