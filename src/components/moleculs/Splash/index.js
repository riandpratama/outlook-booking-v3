import React from "react";
import { Container, Row, Col } from "reactstrap";
const Splash = () => {
  return (
    <Container
      fluid
      style={{ backgroundColor: "black", height: "100%", minHeight: 800 }}
    >
      <Row>
        <Col className="order-xl-12 mb-12 mb-xl-0 main-content" xl="12">
          <div className="text-center mt-6">
            <img
              alt="..."
              width="100%"
              className="mt-8"
              src={require("../../../assets/local/splash.gif").default}
            />
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Splash;
