import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  Spinner,
} from "reactstrap";
import axios from "axios";
import Header from "../Header";
import { useDispatch } from "react-redux";
import { useHistory, withRouter, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();

const formatDatabase = (date) => {
  var dd = ("0" + date.getDate()).slice(-2);
  var mm = ("0" + (date.getMonth() + 1)).slice(-2);
  var yyyy = date.getFullYear();

  var dateIndo = `${yyyy}-${mm}-${dd}`;

  return dateIndo;
};

const rupiah = (bilangan) => {
  var reverse = bilangan.toString().split("").reverse().join(""),
    ribuan = reverse.match(/\d{1,3}/g);
  ribuan = ribuan.join(".").split("").reverse().join("");

  return ribuan;
};

const formatDate = (date) => {
  var dd = ("0" + date.getDate()).slice(-2);
  var mm = ("0" + (date.getMonth() + 1)).slice(-2);
  var yyyy = date.getFullYear();

  var dateIndo = `${dd}/${mm}/${yyyy}`;

  return dateIndo;
};

const DetailBarbermanService = (props) => {
  // console.log(props);
  const history = useHistory();
  const dispatch = useDispatch();

  const [data, setData] = useState({});
  const [date, setDate] = useState("");
  const [dataService, setDataService] = useState({});
  const [isLoading, setLoading] = useState(false);
  const { service_id } = useParams();

  useEffect(() => {
    const id = props.match.params.id;
    const datalocalstorage = localStorage.getItem("customerOutlook");
    const localstorageParse = JSON.parse(datalocalstorage);
    const token = localstorageParse.token;

    setDate(props.location.state);
    axios
      .get(process.env.REACT_APP_API_PATH + `/schedule/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(async (response) => {
        const result = await response.data.data;
        setData(result);
      })
      .catch((err) => {
        console.log(err);
      });

    axios
      .get(process.env.REACT_APP_API_PATH + `/service/${service_id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(async (response) => {
        const result = await response.data.data;
        setDataService(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [props, service_id]);
  console.log(service_id);
  const onSubmit = () => {
    const datalocalstorage = localStorage.getItem("customerOutlook");
    const localstorageParse = JSON.parse(datalocalstorage);
    const token = localstorageParse.token;
    const customerId = localstorageParse.data.id;

    window.confirm(
      "Dengan ini, saya telah membaca dan menyetujui: \n" +
        "1. Keterlambatan maksimal 10 menit\n" +
        "2. Untuk pembatalan pemesanan harap konfirmasi 5 jam sebelumnya\n" +
        "3. Apabila tidak hadir, kartu member di blokir sementara\n" +
        "4. Untuk membuka blokir kartu harap hub i contact person atau datang ke outlook barbershop\n" +
        "5. Untuk buka blokir tidak dikenakan biaya."
    ) &&
      axios
        .post(
          process.env.REACT_APP_API_PATH +
            `/booking/${data.id}/${customerId}/${service_id}`,
          {
            date: formatDatabase(new Date(date)),
            price: data.barberman.price_discount,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
        .then((response) => {
          setLoading(true);
          const dataFromApi = response.data;
          dispatch({ type: "UPDATE_DATA_BOOKING", payload: dataFromApi });
          history.push("/proof-of-booking");
        })
        .catch((error) => {
          if (error.response.status === 401) {
            toast.error("Maaf, status akun telah terblokir sementara, harap hubungi admin outlook !", {
              position: toast.POSITION.TOP_RIGHT,
            });
          } else if (error.response.status === 409) {
            toast.error("Maaf, booking sudah terisi, harap pilih jam atau waktu yang lain.", {
              position: toast.POSITION.TOP_RIGHT,
            });
          }
        });
  };

  return (
    <>
      <Header />
      <div
        className=""
        style={{
          minHeight: "700px",
          backgroundImage:
            "url(" + require("../../../assets/local/bgMain.png").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "center top",
          backgroundColor: "#252525",
        }}
      >
        <br />
        <Container>
          <Row className="mt-8">
            <Col className="order-xl-12 mb-12 mb-xl-0" xl="12">
              <Card className="button-menu-barberman border-0">
                {!data.id ? (
                  <Container style={{ paddingTop: 50 }}>
                    <Row className="justify-content-center">
                      <Spinner style={{ width: "3rem", height: "3rem" }} />
                    </Row>
                  </Container>
                ) : (
                  <>
                    <Row className="justify-content-center">
                      <Col className="order-lg-6" lg="3">
                        <div className="card-profile-image">
                          <img
                            alt=""
                            className="rounded-circle"
                            style={{
                              backgroundColor: "white",
                              border: "3px solid black",
                            }}
                            src={
                              process.env.REACT_APP_IMAGE_PATH +
                              data.barberman.photo
                            }
                          />
                        </div>
                      </Col>
                    </Row>
                    <CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4 button-menu-detail border-0">
                      <div className="d-flex justify-content-between">
                        <Button
                          className="mr-4"
                          style={{ backgroundColor: "#BC9B64", color: "white" }}
                          onClick={() => history.goBack()}
                        >
                          Back
                        </Button>
                        <Button
                          className="float-right"
                          style={{ backgroundColor: "#BC9B64", color: "white" }}
                          onClick={onSubmit}
                          disabled={isLoading}
                        >
                          {isLoading ? "Loading..." : "Booking"}
                        </Button>
                      </div>
                    </CardHeader>
                    <CardBody className="pt-0 pt-md-4">
                      <Row>
                        <div className="col">
                          <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                            <div>
                              <span className="heading text-white">Harga</span>
                              <span className="description text-white">
                                {service_id === "1"
                                  ? rupiah(data.barberman.price_discount)
                                  : `START FROM ${
                                      dataService.price
                                        ? rupiah(dataService.price)
                                        : 0
                                    }`}{" "}
                                Rupiah
                              </span>
                            </div>
                          </div>
                        </div>
                      </Row>
                      <div
                        style={{
                          borderBottom: "3px solid #BC9B64",
                        }}
                      ></div>
                      <br />
                      <div className="text-center text-white">
                        <h3 className="text-white">
                          {data.barberman.name}
                          <span className="font-weight-light text-white"></span>
                        </h3>
                        <div className="h5 font-weight-300">
                          <span className="text-white">
                            {" "}
                            {data.barberman.location.name}{" "}
                          </span>
                        </div>
                        <div className="h5 mt-4 text-white">
                          {data.day.name} - {formatDate(new Date(date))}
                        </div>
                        <div>
                          <i className="ni education_hat mr-2" />
                          {data.time.time}
                        </div>
                        <hr className="my-4" />
                      </div>
                    </CardBody>
                  </>
                )}
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default withRouter(DetailBarbermanService);
