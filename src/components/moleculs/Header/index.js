import React from "react";
import "./header.scss";
import { Button } from "reactstrap";

const onSubmit = () => {
  localStorage.clear();
  window.location.reload();
};

const Header = () => {
  return (
    <div className="flex-container">
      <div className="flex-container horizontal">
        <div className="flex-item">
          <p className="logo-app">
            <img
              alt="Treatment"
              height="50px"
              src={require("../../../assets/local/logo.png").default}
            />
          </p>
        </div>
        <div className="flex-item">
          <Button color="primary" onClick={onSubmit}>
            <span className="btn-inner--text">LOGOUT</span>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Header;
