import React from "react";

import { Col } from "reactstrap";

const Footer = () => {
  return (
    <footer className="footer">
      <Col xl="6">
        <div className="copyright text-center text-xl-left text-muted">
          All Right Reserved © {new Date().getFullYear()}{" "}
          <p className="font-weight-bold ml-1" rel="noopener noreferrer">
            Outlook Barbershop
          </p>
        </div>
      </Col>
    </footer>
  );
};

export default Footer;
