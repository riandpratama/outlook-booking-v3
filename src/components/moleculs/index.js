import Header from "./Header";
import Footer from "./Footer";
import Schedule from "./Schedule";
import ScheduleTreatment from "./ScheduleTreatment";
import Splash from "./Splash";
import DetailBarbermanService from "./DetailBarbermanService";

export {
  Header,
  Footer,
  Schedule,
  ScheduleTreatment,
  Splash,
  DetailBarbermanService,
};
