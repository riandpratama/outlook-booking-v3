import React from "react";

import { withRouter } from "react-router-dom";
import { DetailBarbermanService } from "../../components";

const DetailTreatment = (props) => {
  return (
    <>
      <DetailBarbermanService props={props} />
    </>
  );
};

export default withRouter(DetailTreatment);
