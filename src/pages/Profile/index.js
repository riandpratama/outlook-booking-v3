import React, { useState, useEffect } from "react";
import { CardBody, Container, Row, Col, Table } from "reactstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Header from "../../components/moleculs/Header";

var datalocalstorage = localStorage.getItem("customerOutlook");
var localstorageParse = JSON.parse(datalocalstorage);
var token = localstorageParse.token;

const Profile = () => {
  const history = useHistory();

  const [dataCustomer, setDataCustomer] = useState([]);

  const datalocalstorage = localStorage.getItem("customerOutlook");
  const localstorageParse = JSON.parse(datalocalstorage);
  const {
    id,
    name,
    qrcode,
    address,
    phone,
  } = localstorageParse.data;
  const convertToRupiah = (angka) => {
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for (var i=0; i < angkarev.length; i++) if (i%3 === 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('', rupiah.length-1).reverse().join('');
  }

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_API_PATH + `/customer/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(async (response) => {
        const result = await response.data.data;
        setDataCustomer(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [id]);
  console.log(dataCustomer.hasOwnProperty('coupons') , dataCustomer.coupons)
  return (
    <>
      <Header />
      <div
        className="pb-8 pt-5 pt-lg-8 d-flex align-items-center"
        style={{
          minHeight: "700px",
          backgroundImage:
            "url(" + require("../../assets/local/bgMain.png").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "center top",
          backgroundColor: "#252525",
        }}
      >
        <span className="mask opacity-8" />
        <Container fluid>
          <Row>
            <Col lg="12" md="12">
              <img
                alt="Back"
                height="25px"
                src={require("../../assets/local/backFull.png").default}
                onClick={() => history.goBack()}
              />
            </Col>
            <Col className="mt-5">
              <CardBody className="pt-0 text-white">
                <div className="text-center">
                  <h3 className="text-white">ID QRCODE - {qrcode}</h3>
                </div>
                <Row>
                  <div className="col">
                    <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                      <div>
                        <span className="heading">
                        {dataCustomer.length === 0 ? (
                          0
                        ) : (
                          convertToRupiah(dataCustomer.rupiah_currently)
                        )}</span>
                        <span className="description text-white">
                          KUPON SAAT INI
                        </span>
                      </div>
                      <div>
                        <span className="heading">{dataCustomer.rupiah_exchange}</span>
                        <span className="description text-white">
                          TOTAL PENUKARAN
                        </span>
                      </div>
                    </div>
                  </div>
                </Row>
                <div className="text-center">
                  <h3 className="text-white">
                    {name}
                    <span className="font-weight-light"></span>
                  </h3>
                  <div className="h5 font-weight-300 text-white">
                    <i className="ni location_pin mr-2" />
                    {address}
                  </div>
                  <div className="h5 font-weight-300 text-white">
                    <i className="ni location_pin mr-2" />
                    {phone}
                  </div>
                  <br />
                  <span className="align-items-left">
                    Riwayat Transaksi
                  </span>
                  <div
                    style={{
                      borderBottom: "3px solid #BC9B64",
                      padding: "10px",
                    }}
                  ></div>
                  <Table
                    className="align-items-center table-flush text-white"
                    responsive
                    hover
                  >
                    <thead>
                      <tr>
                        <th>Status</th>
                        <th>Nilai</th>
                        <th>Tanggal</th>
                      </tr>
                    </thead>
                    <tbody>
                      {dataCustomer.hasOwnProperty('coupons') &&
                        dataCustomer.coupons.map((item, index) => (
                          <tr key={index}>
                            <td>{item.status_rupiah === 0 ? 'SCAN' : 'PENUKARAN'}</td>
                            <td>
                              {
                                (item.status_rupiah === 0) ?
                                  '+ ' + convertToRupiah(item.rupiah)
                                :
                                  '- ' + convertToRupiah(item.rupiah)
                              }
                            </td>
                            <td>{new Date(item.createdAt).getDate()}/{new Date(item.createdAt).getMonth()}/{new Date(item.createdAt).getFullYear()}</td>
                          </tr>
                        ))
                      }
                    </tbody>
                  </Table>
                </div>
              </CardBody>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Profile;
