import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { Header } from "../../components";
import "./main.scss";

const Main = () => {
  return (
    <div className="main-app-wrapper">
      <div className="header-wrapper">
        <Header />
      </div>
      <div className="content-wrapper">
        <Router>
          <Switch></Switch>
        </Router>
      </div>
    </div>
  );
};

export default Main;
