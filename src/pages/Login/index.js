import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardBody,
  Container,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
import "./login.scss";
import axios from "axios";
// import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();
const Login = () => {
  const mainContent = React.useRef(null);

  useEffect(() => {
    document.body.classList.add("bg-default");
    return () => {
      document.body.classList.remove("bg-default");
    };
  }, []);

  const [qrcode, setQrcode] = useState("");
  const [phone, setPhone] = useState("");
  const [isLoading, setLoading] = useState(false);
  // const dispatch = useDispatch();

  const onSubmit = () => {
    axios
      .post(process.env.REACT_APP_API_PATH + `/auth/login`, { qrcode, phone })
      .then(async (response) => {
        setLoading(true);
        const dataFromApi = await response.data;
        // dispatch({ type: "UPDATE_DATA_CUSTOMER", payload: dataFromApi.data });
        localStorage.setItem("customerOutlook", JSON.stringify(dataFromApi));
        window.location.reload();
      })
      .catch(() => {
        toast.error("ID QRCODE atau Nomor HP tidak cocok !", {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  return (
    <>
      <div
        className="main-content"
        ref={mainContent}
        style={{
          minHeight: "800px",
          backgroundImage:
            "url(" + require("../../assets/local/bgLogin.png").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "center top",
          // backgroundRepeat: "none",
        }}
      >
        <Container style={{ paddingTop: 50 }}>
          <Row className="justify-content-center">
            <img
              alt="..."
              className="justify-content-center text-center"
              src={require("../../assets/local/logo.png").default}
              width="300"
            />
          </Row>
        </Container>
        {/*<div className="header py-7 py-lg-8"></div>*/}
        {/* Page content */}
        <Container className="mt-5 pb-5">
          <Row className="justify-content-center">
            <Col lg="5" md="7">
              <Card className="bg-secondary shadow border-0 card-login-local">
                <CardBody className="px-lg-5 py-lg-5">
                  <div className="text-center text-muted mb-4">
                    <small style={{ color: "white", fontSize: "35px" }}>
                      Log In
                    </small>
                  </div>
                  <Form>
                    <FormGroup className="mb-3">
                      <InputGroup className="input-group-alternative inputan-login">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <img
                              alt="..."
                              className="justify-content-center text-center"
                              src={
                                require("../../assets/local/idqrcodeIcon.png")
                                  .default
                              }
                              width="23"
                            />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder=" ID QRCODE"
                          type="text"
                          required
                          value={qrcode}
                          onChange={(e) => setQrcode(e.target.value)}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup className="input-group-alternative inputan-login">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <img
                              alt="..."
                              className="justify-content-center text-center"
                              src={
                                require("../../assets/local/phoneIcon.png")
                                  .default
                              }
                              width="23"
                            />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder=" Phone"
                          type="string"
                          value={phone}
                          onChange={(e) => setPhone(e.target.value)}
                          required
                        />
                      </InputGroup>
                    </FormGroup>
                    <div className="custom-control custom-control-alternative custom-checkbox">
                      <input
                        className="custom-control-input"
                        id=" customCheckLogin"
                        type="checkbox"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor=" customCheckLogin"
                      >
                        <span style={{ color: "white" }}>Remember me</span>
                      </label>
                    </div>
                    <div className="text-center">
                      <Button
                        className="my-4 btn-block"
                        color="primary"
                        type="button"
                        onClick={onSubmit}
                        disabled={isLoading}
                      >
                        {isLoading ? "Loading..." : "Log in"}
                      </Button>
                    </div>
                  </Form>
                  <p className="text-center">
                    <span
                      style={{
                        color: "white",
                        fontSize: "12px",
                      }}
                    >
                      &copy; {new Date().getFullYear()} Outlook Barbershop{" "}
                    </span>
                    <br />
                    <a
                      href="https://www.outlookbarbershop.com/"
                      style={{
                        color: "white",
                        fontSize: "13px",
                        fontWeight: "bold",
                      }}
                    >
                      {" "}
                      About Us - Service - Contact{" "}
                    </a>
                  </p>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
      {/*<Container style={{ paddingTop: 30 }}>*/}
      {/*  <Row className="justify-content-center">*/}
      {/*    <p style={{ color: "white" }}>*/}
      {/*      &copy; {new Date().getFullYear()} Outlook Barbershop{" "}*/}
      {/*    </p>*/}
      {/*  </Row>*/}
      {/*</Container>*/}
    </>
  );
};

export default Login;
