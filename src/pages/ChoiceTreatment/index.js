import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import { Button, Container, Row, Col, Spinner } from "reactstrap";
import { withRouter } from "react-router-dom";
import axios from "axios";
import Header from "../../components/moleculs/Header";
import "./choiceTretment.scss";

const ChoiceTreatment = (props) => {
  const isGender = props.location.state.state;
  // console.log(isGender)
  const imageTreatment = [
    "iconSmoothing.png",
    "iconColouring.png",
    "iconToning.png",
    "iconOtherService.png",
  ];

  const history = useHistory();
  const [data, setData] = useState();

  useEffect(() => {
    const datalocalstorage = localStorage.getItem("customerOutlook");
    const localstorageParse = JSON.parse(datalocalstorage);
    const token = localstorageParse.token;

    axios
      .get(process.env.REACT_APP_API_PATH + `/service`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(async (response) => {
        const result = await response.data.data;
        setData(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <>
      <Header />
      <div
        className="pb-8 pt-3 pt-lg-8 d-flex align-items-center"
        style={{
          minHeight: "700px",
          backgroundImage:
            "url(" + require("../../assets/local/bgMain.png").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "center top",
          backgroundColor: "#252525",
        }}
      >
        <Container className="d-flex align-items-center" fluid>
          <Row>
            <Col lg="12" md="12">
              <img
                alt="Back"
                height="25px"
                src={require("../../assets/local/backFull.png").default}
                onClick={() => history.goBack()}
              />
            </Col>
            <Col lg="12" md="12" className="mt-3">
              <p className="text-white h2">Silahkan Pilih Service: </p>
            </Col>
            {!data ? (
              <Container style={{ paddingTop: 50 }}>
                <Row className="justify-content-center">
                  <Spinner style={{ width: "3rem", height: "3rem" }} />
                </Row>
              </Container>
            ) : (
              data.map((item, index) => (
                <div
                  className="col-6 col-md-6 col-lg-3 mb-0"
                  style={{ paddingTop: "10px" }}
                  key={index}
                >
                  <Button
                    className="button-menu border-0"
                    onClick={() => history.push(`/treatment/${item.id}`, {state: isGender})}
                    // history.push("/choice-treatment", {state: 'man'})
                  >
                    <span className="btn-inner--icon">
                      <img
                        alt="Treatment"
                        height="100%"
                        width="100%"
                        src={
                          require(`../../assets/local/${imageTreatment[index]}`)
                            .default
                        }
                      />
                    </span>
                    <div
                      style={
                        imageTreatment[index] === "iconSmoothing.png"
                          ? {
                              borderBottom: "3px solid #BC9B64",
                              padding: "10px",
                              marginBottom: "15px",
                            }
                          : {
                              borderBottom: "3px solid #BC9B64",
                              padding: "10px",
                            }
                      }
                    ></div>
                    <br />
                    <span
                      className="btn-inner--text text-white"
                      style={
                        imageTreatment[index] === "iconColouring.png" ||
                        imageTreatment[index] === "iconOtherService.png"
                          ? { fontSize: "13px" }
                          : { fontSize: "15px", paddingTop: "20px" }
                      }
                    >
                      {item.name}
                    </span>
                    {imageTreatment[index] === "iconColouring.png" ? (
                      <br />
                    ) : (
                      ""
                    )}
                  </Button>
                </div>
              ))
            )}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default withRouter(ChoiceTreatment);
