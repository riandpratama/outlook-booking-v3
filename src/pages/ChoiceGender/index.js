import React from "react";
import { useHistory } from "react-router-dom";

import { Button, Container, Row, Col } from "reactstrap";
import Header from "../../components/moleculs/Header";
import "./choiceGender.scss";

const ChoiceGender = () => {

  const history = useHistory();
  var pathName = history.location.pathname;

  const onLinkHaircutMan = () => {
    history.push("/haircut/1", {state: 'man'});
  };
  const onLinkHaircutWoman = () => {
    history.push("/haircut/1", {state: 'woman'});
  };

  const onLinkTreatmentMan = () => {
    history.push("/choice-treatment", {state: 'man'})
  }
  const onLinkTreatmentWoman = () => {
    history.push("/choice-treatment", {state: 'woman'})
  }

  return (
    <>
      <Header />
      <div
        className="pb-8 pt-3 pt-lg-8 d-flex align-items-center"
        style={{
          minHeight: "700px",
          backgroundImage:
            "url(" + require("../../assets/local/bgMain.png").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "center top",
          backgroundColor: "#252525",
        }}
      >

        <Container className="d-flex align-items-center" fluid>
        { pathName === '/gender/1' ? (
          <Row>
            <Col lg="12" md="12">
              <img
                alt="Back"
                height="25px"
                src={require("../../assets/local/backFull.png").default}
                onClick={() => history.push(`/`)}
              />
            </Col>
            <Col lg="12" md="12" className="mt-3">
              <p className="text-white h2">Silahkan Pilih Gender Anda: </p>
            </Col>
            <br/><br/><br/><br/>
            <div
              className="col-6 col-md-6 col-lg-3 mb-0 pt-50"
              style={{ paddingTop: "10px" }}
            >
              <Button
                className="button-menu border-0"
                onClick={onLinkHaircutMan}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="PRIA"
                    height="100%"
                    width="100%"
                    src={
                      require(`../../assets/local/man.png`)
                        .default
                    }
                  />
                </span>
                <br/><br/>
                <span
                  className="btn-inner--text text-white"
                  style={{ fontSize: "15px", paddingTop: "20px" }}
                >
                PRIA
                </span>
              </Button>
            </div>

            <div
              className="col-6 col-md-6 col-lg-3 mb-0"
              style={{ paddingTop: "10px" }}
            >
              <Button
                className="button-menu border-0"
                onClick={onLinkHaircutWoman}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="Wanita"
                    height="100%"
                    width="100%"
                    src={
                      require(`../../assets/local/woman.png`)
                        .default
                    }
                  />
                </span>
                <br/><br/>
                <span
                  className="btn-inner--text text-white"
                  style={{ fontSize: "15px", paddingTop: "20px" }}
                >
                WANITA
                </span>
              </Button>
            </div>
          </Row>
          )
          :
          <Row>
            <Col lg="12" md="12">
              <img
                alt="Back"
                height="25px"
                src={require("../../assets/local/backFull.png").default}
                onClick={() => history.push(`/`)}
              />
            </Col>
            <Col lg="12" md="12" className="mt-3">
              <p className="text-white h2">Silahkan Pilih Gender Anda: </p>
            </Col>
            <br/><br/><br/><br/>
            <div
              className="col-6 col-md-6 col-lg-3 mb-0 pt-50"
              style={{ paddingTop: "10px" }}
            >
              <Button
                className="button-menu border-0"
                onClick={onLinkTreatmentMan}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="PRIA"
                    height="100%"
                    width="100%"
                    src={
                      require(`../../assets/local/man.png`)
                        .default
                    }
                  />
                </span>
                <br/><br/>
                <span
                  className="btn-inner--text text-white"
                  style={{ fontSize: "15px", paddingTop: "20px" }}
                >
                PRIA
                </span>
              </Button>
            </div>

            <div
              className="col-6 col-md-6 col-lg-3 mb-0"
              style={{ paddingTop: "10px" }}
            >
              <Button
                className="button-menu border-0"
                onClick={onLinkTreatmentWoman}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="Treatment"
                    height="100%"
                    width="100%"
                    src={
                      require(`../../assets/local/woman.png`)
                        .default
                    }
                  />
                </span>
                <br/><br/>
                <span
                  className="btn-inner--text text-white"
                  style={{ fontSize: "15px", paddingTop: "20px" }}
                >
                WANITA
                </span>
              </Button>
            </div>
          </Row>
        }
        </Container>
      </div>
    </>
  );
};

export default ChoiceGender;
