import React from "react";
import background from "../assets/img/theme/profile-cover.jpg";
import Dropdown from "react-bootstrap/Dropdown";
import { Link } from "react-router-dom";

function Homes() {
  return (
    <div class="main-content" id="panel">
      <nav className="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <Dropdown>
              <Dropdown.Toggle variant="primary" id="dropdown-basic">
                User Lorem Ipsum
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item href="#/action-1">Logout</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
      </nav>

      <div
        className="header pb-6 d-flex align-items-center"
        style={{
          minHeight: "600px",
          backgroundImage: `url(${background})`,
          backgroundSize: "cover",
          backgroundPosition: "center top",
        }}
      >
        <span className="mask bg-gradient-default opacity-8"></span>
        <div className="container-fluid d-flex align-items-center">
          <div className="row">
            <div className="col-lg-7 col-md-10">
              <h1 className="display-2 text-white">Hello Jesse</h1>
              <p className="text-white mt-0 mb-5">
                This is your profile page. You can see the progress you've made
                with your work and manage your projects or assigned tasks
              </p>
              <Link to="/#/haircut">
                <a className="btn btn-neutral">Hair Cut</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Homes;

//background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top
