import Main from "./Main";
import Login from "./Login";
import Home from "./Home";
import Profile from "./Profile";
import Haircut from "./Haircut";
import Treatment from "./Treatment";
import ChoiceTreatment from "./ChoiceTreatment";
import DetailHaircut from "./DetailHaircut";
import DetailTreatment from "./DetailTreatment";
import ProofBooking from "./ProofBooking";
import ChoiceGender from "./ChoiceGender";

export {
  Main,
  Login,
  Home,
  Profile,
  Haircut,
  Treatment,
  ChoiceTreatment,
  DetailHaircut,
  DetailTreatment,
  ProofBooking,
  ChoiceGender
};
