import React, { useState } from "react";
import background from "../assets/img/theme/profile-cover.jpg";
import imageBoostrap from "../assets/img/theme/bootstrap.jpg";
import imageAngular from "../assets/img/theme/angular.jpg";
import imageReact from "../assets/img/theme/react.jpg";
import Dropdown from "react-bootstrap/Dropdown";
import Schedule from "../components/ScheduleOld";

function handleScroll() {
  window.scroll({
    top: document.body.offsetHeight,
    left: 0,
    behavior: "smooth",
  });
}

function HairCuts() {
  const [showResults, setShowResults] = useState(false);
  const [name, setName] = useState("");
  const [dataId, setDataId] = useState(0);

  const setActiveTutorial = (nameProp, idData) => {
    setName(nameProp);
    setDataId(idData);
    setShowResults(true);
  };
  return (
    <div className="main-content" id="panel">
      <nav className="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <Dropdown>
              <Dropdown.Toggle variant="primary" id="dropdown-basic">
                User Lorem Ipsum
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item href="#/action-1">Logout</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
      </nav>

      <div
        className="header pb-6 d-flex align-items-center"
        style={{
          minHeight: "100px",
          backgroundImage: `url(${background})`,
          backgroundSize: "cover",
          backgroundPosition: "center top",
        }}
      >
        <span className="mask bg-gradient-default opacity-8"></span>
        <div className="container-fluid d-flex align-items-center">
          <div className="row">
            <div className="col-lg-7 col-md-10">
              <h1 className="display-2 text-white">Hello Jesse</h1>
              <p className="text-white mt-0 mb-5">This is your profile page.</p>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--6">
        <div className="row">
          <div className="col-xl-3 order-xl-1">
            <div className="card">
              <div className="card-header">
                <div className="row align-items-center">
                  <div className="col-8">
                    <h3 className="mb-0">Barberman</h3>
                  </div>
                  <div className="col-4 text-right">
                    <a href="#!" className="btn btn-sm btn-primary"></a>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="table-responsive">
                  <table className="table align-items-center table-flush">
                    <tbody className="list">
                      <tr>
                        <th scope="row">
                          <div
                            className="media align-items-center"
                            onClick={handleScroll}
                          >
                            <a className="avatar rounded-circle mr-3">
                              <img
                                alt="Image placeholder"
                                src={imageBoostrap}
                              />
                            </a>
                            <div
                              className="media-body"
                              // onClick={() => {
                              //   setDataId(1);
                              //   setName("Bootsrap");
                              //   setShowResults(true);
                              // }}
                              onClick={() => setActiveTutorial("Bootsrap", 1)}
                              style={{ cursor: "pointer" }}
                            >
                              <span className="name mb-0 text-sm">
                                Bootstrap Design System
                              </span>
                            </div>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <th scope="row">
                          <div
                            className="media align-items-center"
                            onClick={handleScroll}
                          >
                            <a className="avatar rounded-circle mr-3">
                              <img alt="Image placeholder" src={imageAngular} />
                            </a>
                            <div
                              className="media-body"
                              onClick={() => {
                                setDataId(2);
                                setName("Angular");
                                setShowResults(true);
                              }}
                              style={{ cursor: "pointer" }}
                            >
                              <span className="name mb-0 text-sm">
                                Angular Design System
                              </span>
                            </div>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <th scope="row">
                          <div
                            className="media align-items-center"
                            onClick={handleScroll}
                          >
                            <a className="avatar rounded-circle mr-3">
                              <img alt="Image placeholder" src={imageReact} />
                            </a>
                            <div
                              className="media-body"
                              onClick={() => {
                                setName("ReactJS");
                                setShowResults(true);
                              }}
                              style={{ cursor: "pointer" }}
                            >
                              <span className="name mb-0 text-sm">
                                React Design System
                              </span>
                            </div>
                          </div>
                        </th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xl-9 order-xl-2">
            <div className="card">
              <div className="card-header">
                <div className="row align-items-center">
                  <div className="col-8">
                    <h3 className="mb-0">Schedule Booking </h3>
                  </div>
                  <div className="col-4 text-right">
                    <a href="#!" className="btn btn-sm btn-primary">
                      Settings
                    </a>
                  </div>
                </div>
              </div>
              <div>
                {showResults ? <Schedule name={name} dataId={dataId} /> : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HairCuts;
