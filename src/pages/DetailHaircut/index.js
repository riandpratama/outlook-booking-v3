import React from "react";

import { withRouter } from "react-router-dom";
import "./detailhaircut.scss";
import { DetailBarbermanService } from "../../components";

const DetailHaircut = (props) => {
  return (
    <>
      <DetailBarbermanService props={props} />
    </>
  );
};

export default withRouter(DetailHaircut);
