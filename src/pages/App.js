import React, { useState, useEffect } from "react";
import "../assets/plugins/nucleo/css/nucleo.css";
import "../assets/scss/argon-dashboard-react.scss";
import { Routes, store } from "./../config";
import { Provider } from "react-redux";
import { Alert } from "reactstrap";
import { Splash } from "../components";

function App() {
  const [offlineStatus, setOfflineStatus] = useState(!navigator.onLine);
  const [isLoading, setIsLoading] = useState(true);

  function handleOfflineStatus() {
    setOfflineStatus(!navigator.onLine);
  }

  useEffect(() => {
    handleOfflineStatus();
    window.addEventListener("online", handleOfflineStatus);
    window.addEventListener("offline", handleOfflineStatus);

    setTimeout(() => {
      setIsLoading(false);
    }, 1500);

    return function () {
      window.removeEventListener("online", handleOfflineStatus);
      window.removeEventListener("offline", handleOfflineStatus);
    };
  }, [offlineStatus]);

  return (
    <Provider store={store}>
      {isLoading === true ? (
        <Splash />
      ) : (
        <>
          {offlineStatus && (
            <Alert className="alert-default">
              You're offline. Don't worry, you still can do things.
            </Alert>
          )}
          <Routes />
        </>
      )}
    </Provider>
  );
}

export default App;
