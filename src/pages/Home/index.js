import React,  {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";

import { Button, Container, Row, Col } from "reactstrap";
import Login from "../Login";
import Header from "../../components/moleculs/Header";
import "./home.scss";
import axios from "axios";

const Home = ({ customer }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [data, setData] = useState([]);

  // const onLinkHaircut = () => {
  //   history.push("/haircut/1");
  //   dispatch({
  //     type: "UPDATE_DATA_SUBMIT_BARBERMAN",
  //     payload: {},
  //   });
  // };

  const onLinkGenderHairCut = () => {
    history.push("/gender/1");
    dispatch({
      type: "UPDATE_DATA_SUBMIT_BARBERMAN",
      payload: {},
    });
  };
  const onLinkGenderTreatment = () => {
    history.push("/gender/2");
    dispatch({
      type: "UPDATE_DATA_SUBMIT_BARBERMAN",
      payload: {},
    });
  };

  // const onLinkTreatment = () => {
  //   history.push("/choice-treatment");
  //   dispatch({
  //     type: "UPDATE_DATA_SUBMIT_BARBERMAN",
  //     payload: {},
  //   });
  // };

  useEffect(() => {
    const datalocalstorage = localStorage.getItem("customerOutlook");
    const localstorageParse = JSON.parse(datalocalstorage);
    const token = localstorageParse.token;

    axios
      .get(process.env.REACT_APP_API_PATH + `/description`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(async (response) => {
        const result = await response.data.data;
        setData(result);
      })
      .catch((err) => {
        console.log(err);
      });
  },[]);

  if (customer) {
    const datalocalstorage = localStorage.getItem("customerOutlook");
    const localstorageParse = JSON.parse(datalocalstorage);
    const { name } = localstorageParse.data;


    return (
      <>
        <Header />
        <div
          className="pb-8 pt-2 pt-lg-8 d-flex align-items-center"
          style={{
            minHeight: "700px",
            backgroundImage:
              "url(" + require("../../assets/local/bgMain.png").default + ")",
            backgroundSize: "cover",
            backgroundPosition: "center top",
            backgroundColor: "#252525",
          }}
        >
          <Container className="d-flex align-items-center" fluid>
            <Row>
              <Col lg="12" md="12">
                <h1 className="display-2 text-white">{data.top_desc}</h1>
                <p
                  className="text-white mt-0 mb-1"
                  style={{ fontSize: "14px" }}
                >
                  Hello <b>{name}</b>,
                  <br />
                  {data.middle_desc}
                  <br />
                </p>
                <p
                  className=" mt-0 mb-1"
                  style={{
                    fontSize: "16px",
                    color: "#BC9B64",
                    fontWeight: "bold",
                  }}
                >
                  {data.bottom_desc}
                </p>
                <p className="mt-3 mb-5">
                  <Button
                    color="secondary"
                    onClick={() => history.push("/profile")}
                  >
                    <span className="btn-inner--text">Check Your Profile</span>
                  </Button>
                </p>
              </Col>
              <div className="col-6 col-md-6 col-lg-3 mb-0">
                <Button
                  className="button-menu border-0"
                  onClick={onLinkGenderHairCut}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="Haircut"
                      height="100%"
                      width="100%"
                      src={
                        require("../../assets/local/iconhaircut.png").default
                      }
                    />
                  </span>
                  <div
                    style={{
                      borderBottom: "3px solid #BC9B64",
                      padding: "10px",
                    }}
                  ></div>
                  <br />
                  <span
                    className="btn-inner--text text-white"
                    style={{ fontSize: "20px" }}
                  >
                    Haircut
                  </span>
                  <br />
                  <span style={{ fontSize: "10px" }}>DISC UP TO 15%</span>
                </Button>
              </div>

              <div className="col-6 col-md-6 col-lg-3 mb-0">
                <Button
                  className="button-menu border-0"
                  onClick={onLinkGenderTreatment}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="Treatment"
                      height="100%"
                      width="100%"
                      src={
                        require("../../assets/local/icontreatmen.png").default
                      }
                    />
                  </span>
                  <div
                    style={{
                      borderBottom: "3px solid #BC9B64",
                      padding: "10px",
                    }}
                  ></div>
                  <br />
                  <span
                    className="btn-inner--text text-white"
                    style={{ fontSize: "20px" }}
                  >
                    Treatment
                  </span>
                  <br />
                  <br />
                </Button>
              </div>
            </Row>
          </Container>
        </div>
      </>
    );
  }

  return (
    <>
      <Login />
    </>
  );
};

export default Home;
