import React from "react";
import "./proof-booking.scss";
import { Button, Card, CardBody, Container, Row, Col, Table } from "reactstrap";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

const rupiah = (bilangan) => {
  var reverse = bilangan.toString().split("").reverse().join(""),
    ribuan = reverse.match(/\d{1,3}/g);
  ribuan = ribuan.join(".").split("").reverse().join("");

  return ribuan;
};

const formatDate = (date) => {
  var dd = date.getDate();
  var mm = date.getMonth() + 1;
  var yyyy = date.getFullYear();

  var dateIndo = `${dd}/${mm}/${yyyy}`;

  return dateIndo;
};

const ProofBooking = () => {
  const history = useHistory();

  const stateGlobal = useSelector((state) => state.dataBooking);
  console.log(stateGlobal);

  return (
    <>
      <div
        className=""
        style={{
          minHeight: "700px",
          backgroundImage:
            "url(" + require("../../assets/local/bgMain.png").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "center top",
          backgroundColor: "#252525",
        }}
      >
        <span className="mask opacity-8" />
        <Container className="d-flex align-items-center" fluid>
          <Row className="mt-3">
            <Col className="order-xl-12 mb-12 mb-xl-0" xl="12">
              <Card className="button-menu border-0">
                <CardBody className="pt-0 pt-md-4">
                  <Row>
                    <div className="col">
                      <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                        <div>
                          <span
                            className="heading text-white"
                            style={{ fontSize: "22px" }}
                          >
                            IDR {rupiah(stateGlobal.data.price)}
                          </span>
                          <span
                            className="description text-white"
                            style={{ fontSize: "18px" }}
                          >
                            Rupiah
                          </span>
                        </div>
                      </div>
                    </div>
                  </Row>
                  <div
                    style={{
                      borderBottom: "3px solid #BC9B64",
                    }}
                  ></div>
                  <Table
                    className="align-items-center table-flush text-white"
                    responsive
                  >
                    <tbody>
                      <tr>
                        <th scope="row">Tanggal Pemesanan</th>
                        <td>
                          <i className="fas fa-arrow-up text-success mr-3" />{" "}
                          {formatDate(new Date(stateGlobal.data.createdAt))}
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Tanggal Booking</th>
                        <td>
                          <i className="fas fa-arrow-up text-success mr-3" />{" "}
                          {formatDate(new Date(stateGlobal.data.date))}
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Jam Booking</th>
                        <td>
                          <i className="fas fa-arrow-up text-success mr-3" />{" "}
                          {stateGlobal.dataTime.time}
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Barberman</th>
                        <td>
                          <i className="fas fa-arrow-up text-success mr-3" />{" "}
                          {stateGlobal.dataBarberman.name}
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Service</th>
                        <td>
                          <i className="fas fa-arrow-up text-success mr-3" />{" "}
                          {stateGlobal.dataService.name}
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">Lokasi</th>
                        <td>
                          <i className="fas fa-arrow-up text-success mr-3" />
                          {stateGlobal.dataLocation.name.substring(0, 9)}
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                  <div
                    style={{
                      borderBottom: "3px solid #BC9B64",
                    }}
                  ></div>
                  <div className="text-center text-white">
                    <div>
                      <i className="ni education_hat mr-2 text-white" />
                      <p style={{ fontSize: "18px", fontWeight: "bold" }}>
                        {" "}
                        Harap Tunjukkan Bukti Booking{" "}
                      </p>
                    </div>
                    <div
                      style={{
                        borderBottom: "3px solid #BC9B64",
                      }}
                    ></div>
                    <br />
                    <p>
                      Terimakasih telah melakukan pemesanan secara online untuk
                      mengurangi kepadatan tempat antrian (kerumunan). Juga ikut
                      membantu menjaga protokol kesehatan. <br />
                      <span style={{ fontWeight: "bold", fontSize: "18px" }}>
                        Salam sehat Outlook Barbershop.
                      </span>
                    </p>
                  </div>
                  <Button
                    className="mr-4 btn-block"
                    color="primary"
                    onClick={() => history.push("/")}
                  >
                    SELESAI
                  </Button>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default ProofBooking;
