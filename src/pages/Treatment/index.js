import React from "react";

import { Card, Table, Container, Row, Col } from "reactstrap";
import Header from "../../components/moleculs/Header";

import { ScheduleTreatment } from "../../components";
import { withRouter } from "react-router-dom";

import BarbermanSelect from "../../components/moleculs/BarbermanSelect";
import BarbermanItem from "../../components/moleculs/BarbermanItem";
import { useSelector } from "react-redux";

const Treatment = (props) => {
  const stateGlobal = useSelector((state) => state.dataSubmitBarberman);
  const isGender = props.location.state.state;
  console.log(isGender)
  return (
    <>
      <Header />
      <div
        className=""
        style={{
          minHeight: "700px",
          backgroundImage:
            "url(" + require("../../assets/local/bgMain.png").default + ")",
          backgroundSize: "cover",
          backgroundPosition: "center top",
          backgroundColor: "#252525",
        }}
      >
        <BarbermanSelect barbermanService="2" genderAvailable={isGender} />
        <Container fluid>
          <Row className="mt-1">
            <BarbermanItem barbermanService="2"/>
            <Col className="mb-5 mb-xl-0 mt-3" xl="9">
              <Card className="button-menu ">
                <h2 className="pl-3 pt-3 text-white">
                  {!stateGlobal.name ? (
                    <span>Pilih Barberman</span>
                  ) : (
                    <span>Jadwal {stateGlobal.name}</span>
                  )}
                </h2>
                <div
                  style={{
                    borderBottom: "3px solid #BC9B64",
                  }}
                ></div>
                <Table className="text-center table-flush" responsive>
                  <thead className="button-menu-barberman">
                    <tr>
                      <th scope="col">&nbsp;</th>
                      <th scope="col">
                        <span className="btn btn-danger"></span>{" "}
                        <span style={{ color: "white" }}> : Booked</span>
                      </th>
                      <th scope="col">
                        <span className="btn btn-secondary"></span>{" "}
                        <span style={{ color: "white" }}> : Available</span>
                      </th>
                    </tr>
                  </thead>
                </Table>
                {stateGlobal.result ? (
                  <ScheduleTreatment
                    name={stateGlobal.name}
                    dataId={stateGlobal.id}
                  />
                ) : null}
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default withRouter(Treatment);
