import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import {
  Home,
  Haircut,
  Profile,
  Treatment,
  DetailHaircut,
  ChoiceTreatment,
  ProofBooking,
  DetailTreatment,
  ChoiceGender,
} from "../../pages";

const isLogged = localStorage.getItem("customerOutlook");

const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => {
        if (isLogged) {
          return children;
        } else {
          return <Redirect to="/" />;
        }
      }}
    />
  );
};

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home customer={isLogged} />
        </Route>
        <PrivateRoute exact path="/profile">
          <Profile />
        </PrivateRoute>
        <PrivateRoute exact path="/haircut/:id">
          <Haircut />
        </PrivateRoute>
        <PrivateRoute exact path="/haircut/detail/:id/:service_id">
          <DetailHaircut />
        </PrivateRoute>
        <PrivateRoute path="/choice-treatment">
          <ChoiceTreatment />
        </PrivateRoute>
        <PrivateRoute path="/treatment/:id">
          <Treatment />
        </PrivateRoute>
        <PrivateRoute path="/detail-treatment/:id/:service_id">
          <DetailTreatment />
        </PrivateRoute>
        <PrivateRoute exact path="/proof-of-booking">
          <ProofBooking />
        </PrivateRoute>
        <PrivateRoute exact path="/gender/:id">
          <ChoiceGender />
        </PrivateRoute>
      </Switch>
    </Router>
  );
};

export default Routes;
