const { createStore } = require("redux");

const initialState = {
  dataBooking: {},
  dataBarberman: {},
  dataSubmitBarberman: {},
};

const reducer = (state = initialState, action) => {
  if (action.type === "UPDATE_DATA_BOOKING") {
    return {
      ...state,
      dataBooking: action.payload,
    };
  }

  if (action.type === "UPDATE_DATA_BARBERMAN") {
    return {
      ...state,
      dataBarberman: action.payload,
    };
  }

  if (action.type === "UPDATE_DATA_SUBMIT_BARBERMAN") {
    return {
      ...state,
      dataSubmitBarberman: action.payload,
    };
  }

  return state;
};

const store = createStore(reducer);

export default store;
